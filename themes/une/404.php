<?php get_header(); ?>
<div class="error-404">
        <h1><?php _e('erro', 'une') ?><br><strong>404</strong></h1>
        <p><?php _e('Página não encontrada', 'une') ?></p>
        <a href="/" class="button"> <span><?php _e('Voltar para a página inicial', 'une') ?></span> </a>
    </div>

<?php get_footer(); ?>

<?php get_header(); ?>

<div class="page-content row row-small" id="pageContent">
    <?php the_post(); ?>

    <div class="large-6">
        <div class="column large-12 small-12 text-center mt-40 mb-30">
            <div id="single-the-title">
                <h1 ><?php the_title() ?></h1>
            </div>
        </div>
        <div class="column large-12 small-12 mb-30">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<div class="row page-sidebar">
    <div class="column">
        <?php guaraci\template_part( 'sidebar-widgets', ['sidebar_slug' => 'page']); ?>
    </div>
</div>

<?php get_footer();
<!DOCTYPE html>
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes();?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset');?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1.0">
	<?php wp_head()?>
	<title><?= is_front_page() ? get_bloginfo('name') : wp_title()?></title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/assets/images/favicon.ico?v=2" />
</head>
<body <?php body_class();?> >
	<header class="main-header">
            <div class="sub-header">
                <div class="row">
                    <div class="column large-12">
                        <span class="sub-header--title"> <?= __("União Nacional dos Estudantes") ?> </span>
                        <div class="sub-header--social-icons">
                            <?php une\the_social_networks_menu(false) ?>
                        </div>
                    </div>
                </div> 
            </div>

            <div class="header">
                <div class="row">
                    <div class="column large-12 content-wrapper">
                        
                        <div class="logo">
                            <a href="/" class="logo--wrapper">
                                <?php 
                                $custom_logo_id = get_theme_mod( 'custom_logo' );
                                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                if ( has_custom_logo() ) {
                                        echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '" width="200">';
                                } else {
                                    ?>
                                    <img src="<?= get_template_directory_uri() ?>/assets/images/logo.png" width="200" alt="<?= get_bloginfo( 'name' ) ?>">
                                    <?php
                                }
                                ?>
                            </a>
                        </div>
                        <div class="right-wrapper">
                            <div class="header-menu-wrapper">
                                    <div class="responsive-logo show-medium-down logo">
                                        <a href="#" class="show-medium-down pt-15 pr-0" onclick="jQuery('.responsive-logo, .menu-menu-principal-container').removeClass('active')"><i class="fas fa-times"></i></a>
                                    </div>
                                <a href="javascript:void(0);" class="show-medium-down fz-24 hamburguer-action" onclick="jQuery('.responsive-logo, .menu-menu-principal-container').addClass('active')"><i class="fa fa-bars fz-18"></i></a>
                                <?=wp_nav_menu(['theme_location' => 'main-menu', 'container' => 'nav', 'menu_id' => 'main-menu', 'menu_class' => 'menu'])?>
                            </div>

                            <div class="logo">
                                <a href="/" class="logo--wrapper">
                                    <?php 
                                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                                    $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                    if ( has_custom_logo() ) {
                                            echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '" width="200">';
                                    } else {
                                        ?>
                                        <img src="<?= get_template_directory_uri() ?>/assets/images/logo.png" width="200" alt="<?= get_bloginfo( 'name' ) ?>">
                                        <?php
                                    }
                                    ?>
                                </a>
                            </div>

                            <a href="#" class="join-us-button">
                                <?= __( "Faça Parte" ) ?>
                                <i class="fab fa-font-awesome-flag"></i>
                            </a>
                        
                            <div class="search-form fz-24  d-block text-right">
                                <form action="/">
                                    <input type="text" name="s" id="s" class="search-input-field" placeholder="Pesquisar">
                                </form>
                                <a href="javascript:void(0);" onclick="" id="search-link"><i class="fa fa-search fz-18"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
	</header>
	<div id="app">
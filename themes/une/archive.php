<?php
get_header();
$title = '';
?>
<div class="row mt-10 pt-10 archive-wrapper">
    <div class="column large-12 small-12">
        <?php //guaraci\template_part('search-form') ?>
    </div>
    <?php if(is_author()): $title = sprintf(__('Conteúdo de %s', 'une'), $curauth->display_name) ?>
        <div class="column large-12 small-12 text-center author--info">
            <?= get_avatar($curauth->ID) ?>

            <div>
                <h2 class="author--info-name fz-30 ls-4"><strong><?= $curauth->display_name ?></strong> </h2>
                <div class="author--info-biography"><?= nl2br(get_the_author_meta('description')) ?></div>
            </div>
        </div>
    <?php endif; ?>
    <div class="column medium-9 small-12 ">
        <div class="large-12 small-12">
            <?php guaraci\template_part('archive-title', ['title' => une\get_archive_title()]) ?>
        </div>
        <?php guaraci\template_part('posts-list', ['show_date' => false]); ?>
    </div>

    <div class="column medium-3 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
    
</div>

<?php get_footer();

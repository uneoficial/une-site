<?php
/**
 * Template Name: Página com grid e sidebar
 */
?>

<?php
get_header();
$title = '';
?>
<div class="row mt-10 pt-10 archive-wrapper">
    <?php the_post(); ?>

    <div class="column medium-9 small-12 ">
        <div class="column large-12 small-12 text-center mt-40 mb-30">
            <div id="single-the-title">
                <h1 ><?php the_title() ?></h1>
            </div>
        </div>
        <div class="column large-12 small-12 mb-30">
            <?php the_content(); ?>
        </div>
    </div>

    <div class="column medium-3 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
    
</div>

<?php get_footer();

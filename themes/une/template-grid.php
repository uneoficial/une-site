<?php
/**
 * Template Name: Página com grid e sem título
 */
get_header();
the_post();
?>

<div class="entry row">
    <div class="entry-content column large-12">
        <?php the_content(); ?>
    </div>
</div>

<?php get_footer(); ?>

</div>
<?php wp_reset_postdata() ?>

<footer class="row">
    <div class="column large-12">
        <div class="main-footer">
            <div class="footer-heading">
                <div class="column logo">
                    <a href="/"><img src="<?= get_template_directory_uri() ?>/assets/images/logo-texto.png" alt="Logotipo" height="120"></a>
                </div>

                <div class="column social-area">
                    <div class="title"><?php _e('Siga nossas redes', 'une') ?></div>
                    <div class="d-flex justify-content-between social-networks--wrapper">
                        <?php une\the_social_networks_menu(false) ?>
                    </div>
                </div>


            </div>

            <div class="footer-content">
                <div class="column large-2 medium-12">
                    <?php dynamic_sidebar('footer-area-1'); ?>
                </div>

                <div class="column large-2 medium-12">
                    <?php dynamic_sidebar('footer-area-2'); ?>
                </div>

                <div class="column large-2 medium-12">
                    <?php dynamic_sidebar('footer-area-3'); ?>
                </div>

                <div class="column large-3 medium-12">
                    <?php dynamic_sidebar('footer-area-4'); ?>
                </div>

                <div class="column large-3 medium-12">
                    <?php dynamic_sidebar('footer-area-5'); ?>
                </div>
                

            </div>
            
            <div class="large-12 mt-40 license">
                Todo o conteúdo deste Portal está registrado sob uma Licença Creative Commons – Uso não comercial – Partilha nos mesmos termos 2.5 Brasil (CC BY-NC-SA 2.5)
            </div>


            <div class="large-12 mt-40 byhacklab">
                <?php guaraci\template_part('site-by-hacklab') ?>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>
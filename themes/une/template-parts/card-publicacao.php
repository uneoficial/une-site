<?php

use guaraci\authors;
use guaraci\images;

$show_image = isset($show_image) ? $show_image : false;
$show_excerpt = isset($show_excerpt) ? $show_excerpt : true;
$show_category = isset($show_category) ? $show_category : false;
$show_author = isset($show_author) ? $show_author : true;
$show_date = isset($show_date) ? $show_date : true;
$card_classes = isset($card_classes) ? $card_classes : false;

$horizontal = isset($horizontal) ? $horizontal : false;
$author_taxonomy = isset($author_taxonomy) ? $author_taxonomy : 'autor';

$authors = authors::get($author_taxonomy, false);
$category_str = get_the_category_list(', ');

$image_tag = images::tag('card-large', 'card--image');

$has_image = $show_image && $image_tag;
$uses_excerpt = post_type_supports(get_post_type(), 'excerpt');
$url = null;

?>



<div class="card<?= $horizontal ? ' horizontal' : '' ?><?= $card_classes? ' ' . $card_classes : ''?> mocoes">    
    <?php 
        $url = wp_get_attachment_image_src(get_post_meta( get_the_ID(), 'publi_thumb', true ));
    ?>

    <?php if( $has_image ): ?>        
        <div class="<?= $horizontal ? 'large-4' : '' ?> card-image">
            <div class="card--image-wrapper">
                <?php
                   $pic = $url? '<img src="' . $url[0] . '" class="card--image"/>' : $image_tag;
                    echo $pic;
                ?>
            </div>  
            

            <?php if($show_category): ?>
                <div class="categories">
                    <span class="card--category-title"> 
                        <?php 
                            $term_obj_list = get_the_terms( get_the_ID(), 'publicacao' );
                            $sep_num = 1;

                            foreach ( $term_obj_list as $term ) {
                                echo '<a href="' . get_term_link( $term->slug, 'publicacao' ) . '">' . $term->name . '</a>'; 
                                if ( $sep_num < count($term_obj_list) ) { 
                                    if ( count($term_obj_list) - $sep_num == 1 ) { 
                                        _e( ', ', 'une' );
                                        $sep_num++;
                                    } else {
                                        echo ',';
                                        $sep_num++;
                                    }
                                }
                            }

                        ?>
                    </span>
                </div>
            <?php endif; ?> 

        </div>
    <?php endif ?>
    
    <div class="card--info-wrapper <?= $horizontal ? 'large-8' : 'large-12' ?>">
        <?php if ($show_author && $authors) : ?>
            <span class="authors">
                <?php _e('Por:', 'une') ?>
                <strong><?php authors::display($author_taxonomy) ?></strong>
            </span>
        <?php endif; ?>
        <?php if ($show_date) : ?>
            <span class="date">
                <?php _e('publicado em', 'une') ?>
                <?php echo get_the_date('d/m/Y'); ?>
            </span>
        <?php endif; ?>
        
        <h4 class="card--title">
            <?php the_title() ?>
        </h4>

        <?php if($show_excerpt) : ?>
        <div class="card--excerpt">
            <?= get_post_meta( get_the_ID(), "publi_descricao", true ); ?>
        </div>
        <?php endif; ?>

    </div>

    <a href="<?= wp_get_attachment_url(get_post_meta( get_the_ID(), "publi_arquivo", true )); ?>" download class="download-button">Download</a>
</div>
<?php

use guaraci\authors;
use guaraci\images;

$show_image = isset($show_image) ? $show_image : false;
$show_excerpt = isset($show_excerpt) ? $show_excerpt : true;
$show_category = isset($show_category) ? $show_category : false;
$show_author = isset($show_author) ? $show_author : true;
$show_date = isset($show_date) ? $show_date : true;
$card_classes = isset($card_classes) ? $card_classes : false;
$card_type = isset($card_type) ? $card_type : null;

$horizontal = isset($horizontal) ? $horizontal : false;
$author_taxonomy = isset($author_taxonomy) ? $author_taxonomy : 'autor';

$authors = authors::get($author_taxonomy, false);
$category_str = get_the_category_list(', ');

$image_tag = images::tag('card-large', 'card--image');

$has_image = $show_image && $image_tag;
$uses_excerpt = post_type_supports(get_post_type(), 'excerpt');
$url = null;

?>
<div class="card <?= $horizontal ? 'horizontal' : '' ?> <?= $card_classes ?> <?= $card_type ?>">
    <?php if($show_category && $card_classes == "manchete"): ?>
    <div class="category">
        <?php the_category(', ') ?>
    </div>
    <?php endif; ?>
    
    <?php if( $has_image ): ?>
        
        <div class="<?= $horizontal ? 'large-4' : '' ?> card-image" data-url="<?= $url[0] ?>">
            <a tabindex="-1" href="<?= get_the_permalink() ?>" class="card--image-wrapper">
                <?php
                    $pic = $url? '<img src="' . $url[0] . '" class="card--image"/>' : $image_tag;
                    echo $pic;
                ?>
            </a>    

            <?php if($show_category && has_category() && $card_classes != "manchete"): ?>
                <?php if ( !(count(get_the_category()) == 1 &&  (has_category('Uncategorized') || has_category('Sem categoria')))): ?>
                <div class="categories">
                    <span class="card--category-title"><?php the_category(', ') ?></span>
                </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    <?php endif ?>
    
    <div class="card--info-wrapper <?= $horizontal ? 'large-8' : 'large-12' ?>">
        <?php if ($show_author && $authors) : ?>
            <span class="authors">
                <?php _e('Por:', 'une') ?>
                <strong><?php authors::display($author_taxonomy) ?></strong>
            </span>
        <?php endif; ?>
        <?php if ($show_date) : ?>
            <span class="date">
                <?php _e('publicado em', 'une') ?>
                <?php echo get_the_date('d/m/Y'); ?>
            </span>
        <?php endif; ?>
        
        <h4 class="card--title">
            <a href="<?= get_the_permalink() ?>"><?php the_title() ?></a>
        </h4>

        <?php if($show_excerpt && has_excerpt()) : ?>
        <div class="card--excerpt">
            <a href="<?= get_the_permalink() ?>"><?php the_excerpt() ?></a>
        </div>
        <?php endif; ?>

        <?php if ($card_type == "foto"): ?>
            <div class="date">
                <?= get_post_meta( get_the_ID(), 'memoriaFotos_data', true ) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
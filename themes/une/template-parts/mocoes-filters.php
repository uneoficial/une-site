<div class="filters"> 
            
            <form action="/mocoes-e-resolucoes/" role="search" method="GET" id="filter" class="filters--form">
                <div class="search">
                <input id="" type="text" name="s" autocomplete="off" placeholder="Termo de busca" value="<?= $wp_query->get('s') ?>">
                    <button class="do-search" ><i class="fas fa-search"></i></button>
                </div>

                <h5> FIltre a busca: </h5>


                <div class="item">
                    <span>Formato</span>
                    <select name="formato" id="formato">
                        <option value=""> todos </option>
                        
                        <?php 
                            $terms = get_terms("formato");

                            foreach ($terms as $term): ?>
                                <option value="<?= $term->slug ?>" <?= isset($_GET['formato']) && $_GET['formato'] == $term->slug? 'selected' : '' ?> > <?= $term->name ?> </option>

                        <?php endforeach; ?>

                    </select>
                </div>

                <div class="item">
                    <span>Material</span>
                    <select name="material" id="material">
                        <option value=""> todos </option>
                        <?php 
                            $terms = get_terms("material");

                            foreach ($terms as $term): ?>
                                <option value="<?= $term->slug ?>" <?= isset($_GET['material']) && $_GET['material'] == $term->slug? 'selected' : '' ?>> <?= $term->name ?> </option>

                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="item">
                    <span>Mês</span>
                    <select name="mes" id="mes">
                        <option value=""> todos </option>
                        <?php 
                            $terms = get_terms("mes");

                            foreach ($terms as $term): ?>
                                <option value="<?= $term->slug ?>" <?= isset($_GET['mes']) && $_GET['mes'] == $term->slug? 'selected' : '' ?>> <?= $term->name ?> </option>

                        <?php endforeach; ?>
                    </select>
                </div>
               
                
                <!-- <button class="submit" >Filtrar</button> -->
                
            </form>
            
</div>
<div class="filters"> 
            
            <form action="/publicacoes/" role="search" method="GET" id="filter" class="filters--form">
                <div class="search">
                <input id="" type="text" name="s" autocomplete="off" placeholder="Termo de busca" value="<?= $wp_query->get('s') ?>">
                    <button class="do-search" ><i class="fas fa-search"></i></button>
                </div>

                <h5> FIltre a busca: </h5>

                <div class="item">
                    <span>Tipo de Publicação</span>
                    <select name="publicacao" id="formato">
                        <option value=""> todos </option>
                        
                        <?php 
                            $terms = get_terms("publicacao");
                            

                            foreach ($terms as $term): ?>
                                <option value="<?= $term->slug ?>" <?= isset($_GET['publicacao']) && $_GET['publicacao'] == $term->slug? 'selected' : '' ?> > <?= $term->name ?> </option>

                        <?php endforeach; ?>

                    </select>
                </div>

               
                
                <!-- <button class="submit" >Filtrar</button> -->
                
            </form>
            
</div>
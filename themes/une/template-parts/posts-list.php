<?php
$show_category = isset($show_category) ? $show_category : true;
$show_image = isset($show_image) ? $show_image : true;
$show_excerpt = isset($show_excerpt) ? $show_excerpt : true;

$show_author = isset($show_author) ? $show_author : true;
$show_date = isset($show_date) ? $show_date : true;

$horizontal = isset($horizontal) ? $horizontal : true;
$card_classes = isset($card_classes) ? $card_classes : '';
$card_type = isset($card_type) ? $card_type : null;

$card = isset($card) ? $card : 'card';

?>
<div class="search-content <?= $card ?>">
    <?php

    while (have_posts()) : the_post();
        global $post;
        guaraci\template_part($card, [
            'post' => $post,
            'horizontal' => $horizontal,
            'show_category' => $show_category,
            'show_image' => $show_image,
            'show_excerpt' => $show_excerpt,
            'show_author' => $show_author,
            'show_date' => $show_date,
            'card_classes' => $card_classes,
        ]);
    endwhile ?>
</div>
<div class="pagination-numbers">
    <?php une\pagination_links(); ?>
</div>
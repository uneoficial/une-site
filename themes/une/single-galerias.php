<?php
get_header();
$title = '';
?>
<div class="row mt-10 pt-10 archive-wrapper">
    <div id="" class="modal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
        <a href="#prev" class="prev">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a href="#next" class="next">
            <i class="fas fa-chevron-right"></i>
        </a>
    </div>

    <div class="column medium-9 small-12 ">
        <div class="large-12 small-12">
            <h1 class="mb-20 "> <?php the_title() ?> </h1>
        </div>


        <div class="search-content card-foto">

            <?php

            // check if the repeater field has rows of data
            if( have_rows('galerias_Repeater') ):

                // loop through the rows of data
                while ( have_rows('galerias_Repeater') ) : the_row(); ?>

                    <div class="card foto">
                    
                        <?php 
                            $url = get_sub_field('galerias_Imagem')['url'];
                        ?>

                        
                        <div class="card-image" data-url="<?= $url ?>">
                            <a tabindex="-1" href="<?= get_the_permalink() ?>" class="card--image-wrapper">
                                <?php
                                    $pic = '<img src="' . $url . '" class="card--image"/>';
                                    echo $pic;
                                ?>
                            </a>    

                            <a class="expand">
                                <span class="expand-icon"></span>
                            </a>

                        </div>
                        
                        
                        <div class="card--info-wrapper">                            
                            <h4 class="card--title">
                                <?= get_sub_field('galerias_Legenda'); ?>
                            </h4>
                            
                        </div>

            </div>


                <?php endwhile;

            else :

                // no rows found

            endif;

            ?>
        </div>

    </div>

    <div class="column medium-3 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
    
</div>

<?php get_footer();

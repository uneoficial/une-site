<?php
get_header();
$title = '';

global $wp_query;
if(false) $wp_query = new wp_query;

//$channels = [ 'post', 'outrasmidias', 'blog', 'outrasaude' ];
//$selected_cpts = $wp_query->get('cpts') ?: $channels;

$archive = $wp_query->get('archive');

$action = '';
if($archive){
    $action = get_bloginfo('url') . "/{$archive}/page/1/";

} else if($wp_query->is_search() && !$wp_query->is_author()){
    $action = get_bloginfo('url');
} 

?>
<div class="row mt-10 pt-10 archive-wrapper">
    <div class="column medium-9 small-12 ">
        <div class="large-12 small-12">
            <?php guaraci\template_part('archive-title', ['title' => une\get_archive_title()]) ?>
        </div>

        <?php guaraci\template_part('mocoes-filters') ?>

        <div id="response">
           
        </div>

        <?php guaraci\template_part('posts-list', ['show_date' => false, 'show_category' => true, 'horizontal' => false, 'card' => 'card-moncoes', 'show_excerpt' => true]); ?>
    </div>

    <div class="column medium-3 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
    
</div>

<?php get_footer();

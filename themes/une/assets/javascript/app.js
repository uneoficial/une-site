import Vue from 'vue';

const app = new Vue({
    el: '#app',
})

jQuery(document).ready(function(){
    scrolledMenu();

    if(jQuery('.card-special').length > 0){
        jQuery('.main-header').removeClass('scrolled')
    }

    jQuery('#search-link').click(() => {
        jQuery('.search-form').toggleClass('active ').find('input').focus();
        jQuery('.header-menu-wrapper').toggleClass('disabled-effect');

        if (jQuery('.search-form').find('input').val() != '') {
            jQuery('.search-form').find('form').submit();
        }
    });

    jQuery('.hamburguer-action').click(function() {
        if (jQuery('.search-form').hasClass('active')){
            jQuery('.search-form').removeClass('active');
        }
    });

    jQuery('.search-form a').click(function() {
        if (jQuery('.cross-action, .menu-menu-principal-container').hasClass('active')) {
            jQuery('.cross-action, .menu-menu-principal-container, .responsive-logo').removeClass('active');
        }
    });

    const header = jQuery('header.main-header');
    var didScroll;
    var lastScrollTop = 0;
    var delta = 2;
    var navbarHeight = header.outerHeight();

    // if the url has some archor to go the menu should be hidden
    if (window.location.hash) {
        header.removeClass('nav-up').addClass('nav-down');
    }

    jQuery(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 100);

    function hasScrolled() {
        var currentScrollTop = jQuery(document).scrollTop();

        if(Math.abs(lastScrollTop - currentScrollTop) <= delta)
            return;
    
        if (currentScrollTop > lastScrollTop && currentScrollTop > navbarHeight){
            header.removeClass('nav-down').addClass('nav-up');
        } else {
            if(currentScrollTop + jQuery(window).height() < jQuery(document).height()) {
                header.removeClass('nav-up').addClass('nav-down');
            }
        }
    
        lastScrollTop = currentScrollTop;
    }

    const modal = jQuery(".modal");
    var img = jQuery(".card--image");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    jQuery('.foto .card-image').click( function( e ){
        e.preventDefault();
        modal.css("display", "block");
        modalImg.src = jQuery(this).attr('data-url');
        captionText.innerText = jQuery(this).siblings('.card--info-wrapper').children('.card--title')[0].innerText;

        // if is from kind gallery
        if (jQuery('.single-galerias').length) {
            modal.attr('selected-item', jQuery(this).parent().attr('item-id'));
        }
        
    });

    if (jQuery('.single-galerias').length) {
        let lastItem = 0;

        document.querySelectorAll('.single-galerias .card-foto .card.foto').forEach( ( item, index ) => {
            item.setAttribute('item-id', index);
            lastItem = index;
        } )

        let currentItem = 0;

        jQuery('.next').click( function( e ){
            e.preventDefault();
            currentItem = parseInt(jQuery(this).parent().attr('selected-item'));
            const nextItem =   currentItem + 1 > lastItem? 0 : currentItem + 1;

            modal.attr( 'selected-item', nextItem );
            modalImg.src = jQuery(`.card.foto[item-id=${ nextItem }] .card-image`).attr('data-url');
            captionText.innerText = jQuery(`.card.foto[item-id=${ nextItem }] .card--info-wrapper`)[0].innerText;
        });

        jQuery('.prev').click( function( e ){
            e.preventDefault();
            currentItem = parseInt(jQuery(this).parent().attr('selected-item'));
            const prevItem =   currentItem - 1 < 0? lastItem : currentItem - 1;

            modal.attr( 'selected-item', prevItem );
            modalImg.src = jQuery(`.card.foto[item-id=${ prevItem }] .card-image`).attr('data-url');
            captionText.innerText = jQuery(`.card.foto[item-id=${ prevItem }] .card--info-wrapper`)[0].innerText;
        });

    }


    var span = document.getElementsByClassName("close")[0];

    if (span) {
        span.onclick = function() {
            modal.css("display", "none");
        }
    }

    jQuery('.wpct7 input[type=tel]').
        mask("(99) Z9999-9999", {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
    });

    jQuery('#outro_tipo').hide();

    jQuery('#outro').click(function () {
        jQuery('#outro_tipo').slideDown();
    });

    jQuery('input[type=radio]:not(#outro)').click(function() {
        jQuery('#outro_tipo').slideUp();
        jQuery('#outro').attr('value', jQuery(this).attr('value') )
        
    });

    jQuery('#outro_tipo').on('input',function(e){
        let input_value = jQuery('#outro_tipo').attr('value');
        if (input_value.length == 0) input_value = "outro";

        jQuery("label[for=outro] span.changeble").text( input_value );
        jQuery('#outro').attr('value', input_value)
    });

    // jQuery('#terms').change(function() {
    //     jQuery('.submit-wrapper input').attr('disabled', !jQuery('.submit-wrapper input').attr('disabled'));
    // });

    jQuery('.filters select').change(function() {
        jQuery('#filter').submit();
    });

    if (jQuery(document).width() < 700) {
        //console.log(document.querySelectorAll('.slider-item'));
        document.querySelectorAll('.slider-item').forEach( function( item ) {
            if ( jQuery( item ).attr('data-background').length )  {
                //console.log(123123)
                jQuery( item ).css('background-image', 'url(' + jQuery( item ).attr('data-background') + ')');
            }
        } );

        document.querySelectorAll('.home .members-section .member').forEach( function( item, index ) {
            if ( index > 3 ) {
                jQuery( item ).css('display', 'none');
            }
        } );

        if(jQuery('.home .members-section .see-all').length) {
            jQuery('.home .members-section .see-all').click( function( e ) {
                e.preventDefault();
                document.querySelectorAll('.home .members-section .member').forEach( function( item, index ) {
                    if ( index > 3 ) {
                        jQuery( item ).css('display', 'flex');
                    }
                } );    
            } )
        }

        
    }

    jQuery('.programacao--estados').change(function() {
        jQuery('.programacao--form').submit();
    });


    new Flickity('.slider-section', {
        cellSelector: '.carousel-cell',
        cellAlign: 'center',
        prevNextButtons: false,
        wrapAround: true,
      });

    jQuery('.join-us-button').click(function(e) {
        e.preventDefault();

        if ( jQuery('.formulario-widget').length ) {
            jQuery(document).scrollTop(jQuery('.formulario-widget').offset().top);
        } else {
            jQuery(document).scrollTop(jQuery('.footer-content').offset().top);
        }
    });

})

jQuery(window).scroll(function(){
    scrolledMenu();
})

function scrolledMenu(){
    if(jQuery(window).scrollTop() > 0){
        jQuery('.main-header').addClass('scrolled')
    }else if(jQuery('.card-special').length > 0){
        jQuery('.main-header').removeClass('scrolled')
    }
}
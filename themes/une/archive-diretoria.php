<?php get_header(); ?>

<div class="page-content row row-small" id="centered">
    <?php the_post(); ?>

    <div class="large-6">
        <div class="column large-12 small-12 text-center mt-40 mb-30">
            <div id="single-the-title">
                <h1 >Diretoria</h1>
            </div>
        </div>
    </div>

    <div class="large-9">
        <div class="column large-12 small-12 mb-30">
            <section class="members-section">
                <?php
                $args = array(  
                    'post_type' => 'diretoria',
                    'post_status' => 'publish',
                    'posts_per_page' => -1, 
                );
            
                $loop = new WP_Query( $args ); 
                    
                while ( $loop->have_posts() ) : $loop->the_post();  ?>

                    <div class="member">
                        <div class="pic">
                            <img src="<?= wp_get_attachment_url(get_post_meta( get_the_ID(), 'diretoria_foto', true )); ?>" alt="<?php the_title() ?>" class="avatar">
                        </div>
                        <div class="data">
                            <div class="name"><?php the_title() ?></div>
                            <div class="what"><?= get_post_meta( get_the_ID(), 'diretoria_cargo', true ) ?></div>
                            <div class="description"><?= get_post_meta( get_the_ID(), 'diretoria_profissao_curso', true ) ?></div>
                        </div>
                    </div>

                <?php
                endwhile;
            
                wp_reset_postdata();  ?>

            </section>
        </div>
    </div>
</div>

<div class="row page-sidebar">
    <div class="column">
        <?php guaraci\template_part( 'sidebar-widgets', ['sidebar_slug' => 'page']); ?>
    </div>
</div>

<?php get_footer();

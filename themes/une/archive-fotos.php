<?php
get_header();
$title = '';
?>
<div class="row mt-10 pt-10 archive-wrapper">
    <div id="" class="modal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
    </div>

    <div class="column medium-9 small-12 ">
        <div class="large-12 small-12">
            <?php guaraci\template_part('archive-title', ['title' => une\get_archive_title()]) ?>
        </div>
        <?php guaraci\template_part('posts-list', ['show_date' => false, 'show_category' => false, 'horizontal' => false, 'card' => 'card-foto', 'show_excerpt' => false]); ?>
    </div>

    <div class="column medium-3 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
    
</div>

<?php get_footer();

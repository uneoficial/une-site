<?php 

function custom_post_types() {

/***** [ A UNE ] *****/
// CPT A Une
$labels3 = array(
    'name'                => _x( 'A Une', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Post', 'Post Type Singular Name', 'text_domain' ),
    'all_items'           => __( 'Todos os Posts', 'text_domain' ),
    'menu_name'           => __( 'A Une', 'text_domain' )
);
$args3 = array(
    'label'               => __( 'a-une', 'text_domain' ),
    'labels'              => $labels3,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'a-une', $args3 );

// Diretoria
$labels4 = array(
    'name'            => _x( 'Diretoria', 'Post Type General Name', 'text_domain' ),
    'menu_name'       => __( '» Diretoria', 'text_domain' ),
);
$args4 = array(
    'label'               => __( 'diretoria', 'text_domain' ),
    'labels'              => $labels4,
    'supports'            => array( 'title' ),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=a-une',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'diretoria', $args4 );

// Moções e Resoluções
$labels6 = array(
    'name'                => _x( 'Moções e Resoluções', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Moções e Resoluções', 'text_domain' ),
);

$args6 = array(
    'label'               => __( 'mocoes-e-resolucoes', 'text_domain' ),
    'labels'              => $labels6,
    'supports'            => array( 'title' ),
    'taxonomies'          => array( 'formato', 'material', 'mes' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=a-une',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'mocoes-e-resolucoes', $args6 );


// Downloads
$labels7 = array(
    'name'                => _x( 'Downloads', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Downloads', 'text_domain' ),
);
$args7 = array(
    'label'               => __( 'downloads', 'text_domain' ),
    'labels'              => $labels7,
    'supports'            => array( 'title' ),
    'taxonomies'          => array( 'formato_2', 'material_2', 'mes_2' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=a-une',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'downloads', $args7 );


/***** [ MOVIMENTO ESTUDANTIL ] *****/
// CPT Movimento Estudantil
$labels8 = array(
    'name'                => _x( 'Movimento Estudantil', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Post', 'Post Type Singular Name', 'text_domain' ),
    'all_items'           => __( 'Todos os Posts', 'text_domain' ),
    'menu_name'           => __( 'Movimento Estudantil', 'text_domain' ),
);
$args8 = array(
    'label'               => __( 'movimento-estudantil', 'text_domain' ),
    'labels'              => $labels8,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
);
register_post_type( 'movimento-estudantil', $args8 );

// Agenda do ME
$labels9 = array(
    'name'                => _x( 'Agenda do ME', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Agenda do ME', 'text_domain' ),
);
$args9 = array(
    'label'               => __( 'agenda-do-me', 'text_domain' ),
    'labels'              => $labels9,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=movimento-estudantil',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
);
register_post_type( 'agenda-do-me', $args9 );



/***** [ MEMÓRIA ] *****/
// CPT Memória
$labels12 = array(
    'name'                => _x( 'Memória', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Post', 'Post Type Singular Name', 'text_domain' ),
    'all_items'           => __( 'Todos os Posts', 'text_domain' ),
    'menu_name'           => __( 'Memória', 'text_domain' ),
);
$args12 = array(
    'label'               => __( 'memoria', 'text_domain' ),
    'labels'              => $labels12,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'category' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'memoria', $args12 );

// Presidentes
$labels13 = array(
    'name'                => _x( 'Presidentes', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Presidentes', 'text_domain' ),
);
$args13 = array(
    'label'               => __( 'presidentes', 'text_domain' ),
    'labels'              => $labels13,
    'supports'            => array( 'title', 'editor' ),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=memoria',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'presidentes', $args13 );

// Publicações
$labels14 = array(
    'name'                => _x( 'Publicações', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Publicações', 'text_domain' ),
);
$args14 = array(
    'label'               => __( 'publicacoes', 'text_domain' ),
    'labels'              => $labels14,
    'supports'            => array( 'title' ),
    'taxonomies'          => array( 'publicacao' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=memoria',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'publicacoes', $args14 );

// Fotos
$labels15 = array(
    'name'                => _x( 'Fotos', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Fotos', 'text_domain' ),
);
$args15 = array(
    'label'               => __( 'fotos', 'text_domain' ),
    'labels'              => $labels15,
    'supports'            => array( 'title' ),
    'taxonomies'          => array( 'evento' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=memoria',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'fotos', $args15 );

// Documentos
$labels16 = array(
    'name'                => _x( 'Documentos', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Documentos', 'text_domain' ),
);
$args16 = array(
    'label'               => __( 'documentos', 'text_domain' ),
    'labels'              => $labels16,
    'supports'            => array( 'title' ),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=memoria',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'documentos', $args16 );

// Praia do Flamengo 132
$labels17 = array(
    'name'                => _x( 'Praia do Flamengo', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Praia do Flamengo', 'text_domain' ),
);
$args17 = array(
    'label'               => __( 'praia-do-flamengo', 'text_domain' ),
    'labels'              => $labels17,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'secoes-praia', 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=memoria',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'praia-do-flamengo', $args17 );


/***** [ NOTÍCIAS ] *****/
// CPT Notícias
$labels18 = array(
    'name'                => _x( 'Notícias', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Post', 'Post Type Singular Name', 'text_domain' ),
    'all_items'           => __( 'Todos os Posts', 'text_domain' ),
    'menu_name'           => __( 'Notícias', 'text_domain' ),
);
$args18 = array(
    'label'               => __( 'noticias', 'text_domain' ),
    'labels'              => $labels18,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'show_in_rest'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'noticias', $args18 );

// TV Une
$labels19 = array(
    'name'                => _x( 'TV UNE', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» TV UNE', 'text_domain' ),
);
$args19 = array(
    'label'               => __( 'tv-une', 'text_domain' ),
    'labels'              => $labels19,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'tv-une', $args19 );

// Galerias
$labels20 = array(
    'name'                => _x( 'Galerias', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Galerias', 'text_domain' ),
);
$args20 = array(
    'label'               => __( 'galerias', 'text_domain' ),
    'labels'              => $labels20,
    'supports'            => array( 'title' ),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'galerias', $args20 );

// Cultura Dentro do Bolso
$labels21 = array(
    'name'                => _x( 'Cultura Dentro do Bolso', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Cultura Dentro do Bolso', 'text_domain' ),
);
$rewrite21 = array(
    'slug'                => 'cultura-dentro-do-bolso',
);
$args21 = array(
    'label'               => __( 'culturadentrodobolso', 'text_domain' ),
    'labels'              => $labels21,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'rewrite'             => $rewrite21,
    'capability_type'     => 'post'
);
register_post_type( 'culturadentrodobolso', $args21 );

// Opinião
$labels22 = array(
    'name'                => _x( 'Opinião', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Opinião', 'text_domain' ),
);
$args22 = array(
    'label'               => __( 'opiniao', 'text_domain' ),
    'labels'              => $labels22,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'show_in_rest'        => true,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'opiniao', $args22 );


// Editorial
$labels22 = array(
    'name'                => _x( 'Editorial', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Editorial', 'text_domain' ),
);
$args22 = array(
    'label'               => __( 'editorial', 'text_domain' ),
    'labels'              => $labels22,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_rest'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'editorial', $args22 );

// A UNE Somos Nós
$labels23 = array(
    'name'                => _x( 'A UNE Somos Nós', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» A UNE Somos Nós', 'text_domain' ),
);
$args23 = array(
    'label'               => __( 'a-une-somos-nos', 'text_domain' ),
    'labels'              => $labels23,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'a-une-somos-nos', $args23 );

// Lugar de Mulher
$labels24 = array(
    'name'                => _x( 'Lugar de Mulher', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» Lugar de Mulher', 'text_domain' ),
);
$args24 = array(
    'label'               => __( 'lugar-de-mulher', 'text_domain' ),
    'labels'              => $labels24,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'lugar-de-mulher', $args24 );

// CUCA da UNE
$labels25 = array(
    'name'                => _x( 'CUCA da UNE', 'Post Type General Name', 'text_domain' ),
    'menu_name'           => __( '» CUCA da UNE', 'text_domain' ),
);
$args25 = array(
    'label'               => __( 'cuca-da-une', 'text_domain' ),
    'labels'              => $labels25,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => 'edit.php?post_type=noticias',
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'cuca-da-une', $args25 );


/***** [ IMPRENSA ] *****/
// CPT Imprensa
$labels26 = array(
    'name'                => _x( 'Imprensa', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Post', 'Post Type Singular Name', 'text_domain' ),
    'all_items'           => __( 'Todos os Posts', 'text_domain' ),
    'menu_name'           => __( 'Imprensa', 'text_domain' ),
);
$args26 = array(
    'label'               => __( 'imprensa', 'text_domain' ),
    'labels'              => $labels26,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'          => array( 'post_tag', 'category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'show_in_rest'        => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'imprensa', $args26 );


// Atividades
$labels_atividade = array(
    'name'                => _x( 'Atividades', 'Post Type General Name', 'une' ),
    'singular_name'       => _x( 'Atividade', 'Post Type Singular Name', 'une' ),
    'all_items'           => __( 'Todas as Atividades', 'une' ),
    'menu_name'           => __( 'Atividades', 'une' )
);
$args_atividade = array(
    'label'               => __( 'atividades', 'une' ),
    'labels'              => $labels_atividade,
    'supports'            => array( 'title', 'editor' ),
    'taxonomies'          => array( '' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 30,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post'
);
register_post_type( 'atividades', $args_atividade );


}


add_action( 'init', 'custom_post_types', 0 );

?>
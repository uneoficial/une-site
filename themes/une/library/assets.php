<?php

namespace une;

function enqueue_assets()
{
    wp_enqueue_style('flickity', 'https://unpkg.com/flickity@2/dist/flickity.min.css');
    wp_enqueue_style('foundation', get_template_directory_uri() . '/dist/foundation.min.css');
    wp_enqueue_style('app', get_template_directory_uri() . '/dist/app.css', [], filemtime(get_template_directory() . '/dist/app.css'));
    wp_enqueue_script( 'estados-e-municipios', get_template_directory_uri() . '/assets/javascript/estados-e-municipios.js');

    wp_enqueue_script( 'jquery-mask', get_template_directory_uri() . '/assets/javascript/jquery.mask.js', ['jquery']);
    wp_enqueue_script('main-app', get_template_directory_uri() . '/dist/app.js', ['jquery'], filemtime(get_template_directory() . '/dist/app.js'), true);
    wp_enqueue_script('cookie', get_template_directory_uri() . '/assets/javascript/js.cookie.js', ['jquery'], false, true);
    wp_enqueue_script('acessibilidade', get_template_directory_uri() . '/assets/javascript/acessibilidade.js', ['jquery', 'cookie'], false, true);
    wp_enqueue_script('youtube-plataform', 'https://apis.google.com/js/platform.js');
    wp_enqueue_script('flickity', 'https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js');

}

add_action('wp_enqueue_scripts', 'une\\enqueue_assets');

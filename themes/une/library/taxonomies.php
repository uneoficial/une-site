<?php

function custom_taxonomy() {



/***** [ MOÇÕES E RESOLUÇÕES ] *****/

// Formato de Arquivo

$labels_1 = array(

    'name'              => _x( 'Formatos', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Formato', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Formato', 'text_domain' ),

);

$args_1 = array(

    'labels'            => $labels_1,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

    // 'rewrite'           => array( 'slug' => 'mocoes-e-resolucoes/formato' ),

);

register_taxonomy( 'formato', array( 'mocoes-e-resolucoes' ), $args_1 );



// Material

$labels_2 = array(

    'name'              => _x( 'Materiais', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Material', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Material', 'text_domain' ),

);

$args_2 = array(

    'labels'            => $labels_2,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'material', array( 'mocoes-e-resolucoes' ), $args_2 );



// Mês

$labels_3 = array(

    'name'              => _x( 'Meses', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Mês', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Mês', 'text_domain' ),

);

$args_3 = array(

    'labels'            => $labels_3,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'mes', array( 'mocoes-e-resolucoes' ), $args_3 );





/***** [ DOWNLOADS ] *****/

// Formato de Arquivo

$labels_4 = array(

    'name'              => _x( 'Formatos', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Formato', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Formato', 'text_domain' ),

);

$args_4 = array(

    'labels'            => $labels_4,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'formato_2', array( 'downloads' ), $args_4 );



// Material

$labels_5 = array(

    'name'              => _x( 'Materiais', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Material', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Material', 'text_domain' ),

);

$args_5 = array(

    'labels'            => $labels_5,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'material_2', array( 'downloads' ), $args_5 );



// Mês

$labels_6 = array(

    'name'              => _x( 'Meses', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Mês', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Mês', 'text_domain' ),

);

$args_6 = array(

    'labels'            => $labels_6,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'mes_2', array( 'downloads' ), $args_6 );





/***** [ MOVIMENTO ESTUDANTIL ] *****/

// Letras

$labels_7 = array(

    'name'              => _x( 'Letras', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Letra', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Letra', 'text_domain' ),

);

$args_7 = array(

    'labels'            => $labels_7,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'letra', array( 'dicionario-do-me' ), $args_7 );





/***** [ MEMÓRIA ] *****/

// Publicações

$labels_8 = array(

    'name'              => _x( 'Tipos de Publicações', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Tipo de Publicação', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Tipo de Publicação', 'text_domain' ),

);

$args_8 = array(

    'labels'            => $labels_8,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'publicacao', array( 'publicacoes' ), $args_8 );



// Fotos

$labels_9 = array(

    'name'              => _x( 'Evento', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Evento', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Evento', 'text_domain' ),

);

$args_9 = array(

    'labels'            => $labels_9,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'evento', array( 'fotos' ), $args_9 );



// Praia do Flamengo

$labels_9 = array(

    'name'              => _x( 'Seções', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Seções', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Seções', 'text_domain' ),

);

$args_9 = array(

    'labels'            => $labels_9,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'secoes-praia', array( 'praia-do-flamengo' ), $args_9 );


// Campanha

$labels_campanha = array(

    'name'              => _x( 'Campanhas', 'Taxonomy General Name', 'text_domain' ),

    'singular_name'     => _x( 'Campanha', 'Taxonomy Singular Name', 'text_domain' ),

    'menu_name'         => __( 'Campanhas', 'text_domain' ),

);

$args_campanha = array(

    'labels'            => $labels_campanha,

    'hierarchical'      => true,

    'public'            => true,

    'show_ui'           => true,

    'show_admin_column' => true,

    'show_in_nav_menus' => true,

    'show_tagcloud'     => true,

);

register_taxonomy( 'campanha', array( 'atividades' ), $args_campanha );


}

add_action( 'init', 'custom_taxonomy', 0 );

?>
<?php 

function adicional_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Coluna 1 - Footer', 'une' ),
        'id' => 'footer-area-1',
        'description' => __( 'Conteúdo da ultima coluna do footer.', 'une' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-title">',
	'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Coluna 2 - Footer', 'une' ),
        'id' => 'footer-area-2',
        'description' => __( 'Conteúdo da coluna do meio footer.', 'une' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-title">',
	'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Coluna 3 - Footer ', 'une' ),
        'id' => 'footer-area-3',
        'description' => __( '', 'une' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="footer-title">',
	'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Coluna 4 - Footer ', 'une' ),
        'id' => 'footer-area-4',
        'description' => __( '', 'une' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="footer-title">',
    'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Coluna 5 - Footer ', 'une' ),
        'id' => 'footer-area-5',
        'description' => __( '', 'une' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="footer-title">',
    'after_title'   => '</h2>',
    ) );
}


add_action( 'widgets_init', 'adicional_widgets_init' );


?>
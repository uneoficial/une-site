<?php
/*
  Widget Name: Listagem de Bandeiras   
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class Bandeiras extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'exibicao' => array(
                'type' => 'select',
                'label' => __( 'Local de exibição', 'une' ),
                'multiple' => false,
                'options' => array(
                    'horizontal' => 'Home',
                    'interna' => 'Interna'
                ),
                'required' => true,
            ),

            'description' => array(
                'type' => 'tinymce',
                'label' => __( 'Descrição', 'une' ),
                'rows' => 10,
                'default_editor' => 'text',
                'button_filters' => array(
                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                ),
            ),

            'repetidor_membro' => array(
                'type' => 'repeater',
                'label' => __( 'Itens' , 'une'),
                'fields' => array(
                    'title' => array(
                        'type' => 'text',
                        'label' => __( 'Title', 'une')
                    ),

                    'link' => array(
                        'type' => 'text',
                        'label' => __( 'Link usado na home', 'une')
                    ),

                    'avatar' => array(
                        'type' => 'media',
                        'label' => __( 'Icone', 'une' ),
                        'choose' => __( 'Escolha uma imagem', 'une'),
                        'update' => __( 'Defina uma imagem', 'une'),
                        'library' => 'image',
                        'fallback' => true
                    ),

                    'description' => array(
                        'type' => 'tinymce',
                        'label' => __( 'Descrição', 'une' ),
                        'rows' => 10,
                        'default_editor' => 'text',
                        'button_filters' => array(
                            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                        ),
                    ),
                ),
            )
        ];

        parent::__construct('bandeiras', 'Listagem de Bandeiras   ', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('bandeiras', __FILE__, 'widgets\Bandeiras');

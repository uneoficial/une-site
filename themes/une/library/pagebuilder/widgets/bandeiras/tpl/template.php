<section class="bandeiras-section <?= $instance['exibicao'] ?>">
        <?php 
            if ($instance['exibicao'] == 'horizontal'):

                
        ?>

        <div class="large-3 info">
                <div class="title">Nossas bandeiras</div>
                <div class="description">
                    <?= $instance['description'] ?>
                </div>
        </div>

        <div class="large-9 content">     
            <?php foreach ($instance['repetidor_membro'] as $block): ?>
                <a href="<?= $block['link'] ?>" class="bandeira">
                    <div class="data">
                        <div class="icon">
                            <img src="<?= wp_get_attachment_url($block['avatar']); ?>" alt="<?= $block['name'] ?>" class="avatar">
                        </div>

                        <div class="title">
                            <h2> <?= $block['title'] ?> </h2>
                        </div>
                    </div>
                    
                    <div class="description"><?= $block['description'] ?></div>
                </a>
            <?php endforeach; ?>
        
        </div>

        <?php else: ?>

        <?php foreach ($instance['repetidor_membro'] as $block): ?>
            <?php 
                $id = null;
                if ( strlen( $block['link'] ) > 0 ) {
                    $link_arr = explode("#", $block['link']);
                    if ( sizeof( $link_arr ) > 1 ) {
                        $id = $link_arr[1];
                    }
                }
            ?>
        
            <div class="bandeira" id="<?= $id ?>">
                <div class="data">
                    <div class="icon">
                        <img src="<?= wp_get_attachment_url($block['avatar']); ?>" alt="<?= $block['name'] ?>" class="avatar">
                    </div>

                    <div class="title">
                        <h2> <?= $block['title'] ?> </h2>
                    </div>
                </div>
                
                <div class="description"><?= $block['description'] ?></div>
                
            </div>
        <?php endforeach; ?>

        <?php endif; ?>
</section>
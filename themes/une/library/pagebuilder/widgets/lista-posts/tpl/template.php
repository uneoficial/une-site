
<div class="post-list">
        <?php if ($instance['title'] != ''): ?>
        <div class="noticias-header">
            <div class="title">
                <h5 class="ff-title"><?php echo $instance['title']; ?></h5>
                <a href="<?= $instance['button_url']; ?>" class="">
                    <i class="fas fa-long-arrow-alt-right"></i>
                    <?= $instance['button_text']; ?>
                </a>
            </div>
        </div>
        <?php endif; ?>

        <?php
            $card = isset($card) ? $card : 'card';
        ?>

        <div class="search-content <?= $instance['css_class'] ?>">
            <?php
            $news_query = new WP_Query( $query_args );
            while ( $news_query->have_posts()) : $news_query->the_post();
                global $post;
                guaraci\template_part($card, [
                    'post' => $post,
                    'horizontal' => false,
                    'show_category' => !$instance['hide_categories'],
                    'show_image' => !$instance['hide_images'],
                    'show_excerpt' => !$instance['hide_excerpt'],
                    'show_author' => false,
                    'show_date' => false,
                    'card_classes' => $instance['css_class'],
                ]);
            endwhile ?>
        </div>
        

        
</div>

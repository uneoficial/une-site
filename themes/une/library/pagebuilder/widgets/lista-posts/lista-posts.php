<?php

/*
  Widget Name: Área de listagem de Posts
  Description: Adiciona uma lista de posts
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class ListaPosts extends \SiteOrigin_Widget {
    function __construct() {    
        $fields = [
            'css_class' => [
                'type' => 'select',
                'label' => __('Selecione o estilo', 'une'),
                'default' => 'horizontal',
                'options' => array(
                    'manchete' => __('Manchete', 'une'),
                    'horizontal' => __('Lista horizontal', 'une'),
                    'list' => __('Lista vertical', 'une'),
                ),
            ],

            'hide_categories' => [
                'type' => 'checkbox',
                'label' => __('Não exibir categorias', 'une'),
            ],

            'hide_images' => [
                'type' => 'checkbox',
                'label' => __('Não exibir imagens', 'une'),
            ],

            'hide_excerpt' => [
                'type' => 'checkbox',
                'label' => __('Não exibir resumo', 'une'),
            ],

            'title' => [
                'type' => 'text',
                'label' => 'Titulo da seção'
            ],

            'conteudo' => [
                'type' => 'posts',
                'label' => 'Conteúdo'
            ],

            'button_text' => array(
                'type' => 'text',
                'label' => __('Texto do botão', 'une'),
                'default' => 'ver todas'
            ),

            'button_url' => array(
                'type' => 'link',
                'label' => __('Link do botão', 'une'),
                'default' => 'http://www.example.com'
            ),
            
        ];

        parent::__construct('lista-posts', "Área de listagem de posts", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Adiciona uma lista de Notícias'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

    function get_template_variables($instance, $args) {
        $posts_query_args = siteorigin_widget_post_selector_process_query($instance['conteudo']);

        return ['query_args' => $posts_query_args];
    }

}

Siteorigin_widget_register('lista-posts', __FILE__, 'widgets\ListaPosts');

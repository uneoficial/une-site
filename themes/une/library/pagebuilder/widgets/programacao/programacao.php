<?php
/*
  Widget Name: Listagem de Programações
  Description: Área que exibe uma listagem de programações
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class Programacao extends \SiteOrigin_Widget {

    function __construct() {

        $filtered_terms = [];

        $terms = get_terms( 'campanha', array(
            'hide_empty' => false,
        ) );

        foreach ($terms as $term) {
            $filtered_terms[$term->slug] = $term->name;
        }

        $fields = [
            'campanha' => array(
                'type' => 'select',
                'label' => __( 'Selecione uma campanha associada', 'une' ),
                'multiple' => false,
                'options' => $filtered_terms,
                'required' => true,
            ),

            'title' => array(
                'type' => 'text',
                'label' => __('Titulo', 'une'),
            ),

            'quantidade' => array( // temporario
                'type' => 'number',
                'label' => __('Quantidade', 'une'),
            ),
        ];

        parent::__construct('programacao', 'Listagem de Programações', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Área que exibe uma listagem de programações'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('programacao', __FILE__, 'widgets\Programacao');

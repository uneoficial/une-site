<?php
    $estado_filtro = isset( $_GET["estado"] ) ? $_GET['estado'] :  'Selecione UF';
?>


<section class="programacao" id="filtroform">
    <div class="title">
        <i class="far fa-calendar"></i>
        <h1>
            <?= $instance['title'] ?>
        </h1>
    </div>

    <div class="filters">
        Selecione um Estado para filtrar a programação
        <form action="#filtroform" method="GET" class="programacao--form">
            <select required="required" name="estado" class="programacao--estados">
                <option value="<?= $estado_filtro ?>"> <?= $estado_filtro ?> </option>
                <option value="AC" code="12">AC</option>
                <option value="AL" code="27">AL</option>
                <option value="AM" code="13">AM</option>
                <option value="AP" code="16">AP</option>
                <option value="BA" code="29">BA</option>
                <option value="CE" code="23">CE</option>
                <option value="DF" code="53">DF</option>
                <option value="ES" code="32">ES</option>
                <option value="GO" code="52">GO</option>
                <option value="MA" code="21">MA</option>
                <option value="MG" code="31">MG</option>
                <option value="MS" code="50">MS</option>
                <option value="MT" code="51">MT</option>
                <option value="PA" code="15">PA</option>
                <option value="PB" code="25">PB</option>
                <option value="PE" code="26">PE</option>
                <option value="PI" code="22">PI</option>
                <option value="PR" code="41">PR</option>
                <option value="RJ" code="33">RJ</option>
                <option value="RN" code="24">RN</option>
                <option value="RO" code="11">RO</option>
                <option value="RR" code="14">RR</option>
                <option value="RS" code="43">RS</option>
                <option value="SC" code="42">SC</option>
                <option value="SE" code="28">SE</option>
                <option value="SP" code="35">SP</option>
                <option value="TO" code="17">TO</option>
            </select>
        </form>
    </div>

    <div class="content">
        <div class="header">
            <div class="large-1 small-3 medium-3">Data</div>
            <div class="large-1 hide-medium-down">Horário</div>
            <div class="large-2 hide-medium-down">Tipo de Atividade</div>
            <div class="large-4 small-6 medium-6">Nome da Atividade</div>
            <div class="large-2 small-3 medium-3">Cidade</div>
            <div class="large-2 hide-medium-down">Universidade</div>
        </div>

        <div class="rows">

            <?php 

            $atv_arg = array(  
                'post_type' => 'atividades',
                'post_status' => 'publish',
                'posts_per_page' => $instance['quantidade'], 
                'orderby' => 'title',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'campanha',
                        'field' => 'slug',
                        'terms' => $instance['campanha'],
                    )
                ),

            );

            if ( $estado_filtro != "Selecione UF" ) {
                $atv_arg['meta_query'] = array(
                    array(
                    'key' => 'uf',
                    'value' => $estado_filtro,
                    )
                );
            }

            $loop = new WP_Query( $atv_arg ); 
                
            while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="item">
                    <div class="large-1 small-3 medium-3"><?= get_field( 'data' ) ?></div>
                    <div class="large-1 hide-medium-down"><?= get_field( 'horario' ) ?></div>
                    <div class="large-2 hide-medium-down"><?= get_field( 'tipo' ) ?></div>
                    <div class="large-4 small-6 medium-6 atividade"><?php the_title() ?></div>
                    <div class="large-2 small-3 medium-3"><?= get_field( 'cidade' ) ?></div>
                    <div class="large-2 hide-medium-down"><?= get_field( 'universidade' ) ?></div>
                </div>
            <?php
            endwhile;

            wp_reset_postdata(); 

            ?>
            
        </div>
    </div>

</section>

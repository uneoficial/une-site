<?php
/*
  Widget Name: Imagem com botão de download
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class ImagemBaixavel extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
                
            'imagem' => array(
                'type' => 'media',
                'label' => __('Imagem', 'une' ),
                'library' => 'image',
            ),

            'url' => array(
                'type' => 'link',
                'label' => __('Link da imagem', 'une'),
                'default' => 'http://www.example.com'
            ),

            'button_text' => array(
                'type' => 'text',
                'label' => __('Texto do botão', 'une'),
                'default' => 'Baixe aqui os arquivos de divulgação da campanha'
            ),

            'button_url' => array(
                'type' => 'link',
                'label' => __('Link do botão de download', 'une'),
                'default' => 'http://www.example.com'
            ),

            'direct_download' => array(
                'type' => 'checkbox',
                'label' => __( 'O link é para um arquivo?', 'une' ),
                'description' => _('Caso selecionado, e o link de destido for um arquivo, o download começará automaticamente sem redirecionamento.'),
                'default' => false,
            ),
            
        ];

        parent::__construct('imagem-baixavel', 'Imagem com botão de download', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Imagem com botão de download'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('imagem-baixavel', __FILE__, 'widgets\ImagemBaixavel');

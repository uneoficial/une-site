
<section class="image-baixavel">
    <a href="<?= $instance['url'] ?>">
        <div class="image">
            <?= wp_get_attachment_image($instance['imagem'],'full') ?>
        </div>
    </a>

    <a href="<?= $instance['button_url'] ?>" class="button download" <?= $instance['direct_download']? 'download' : null?>>
        <span class="save-icon"></span>
        <?= $instance['button_text'] ?>
    </a>

</section>

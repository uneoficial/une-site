<?php
/*
  Widget Name: Últimos tweets
  Description: Listagem de tweets
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class UltimosTuites extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'title' => array(
                'type' => 'text',
                'label' => __('Titulo', 'une'),
            ),
        ];

        parent::__construct('ultimos-tuites', 'Últimos tweets', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Listagem de tweets'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('ultimos-tuites', __FILE__, 'widgets\UltimosTuites');

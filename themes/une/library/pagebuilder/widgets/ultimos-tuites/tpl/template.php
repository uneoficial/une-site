
<section class="ultimos-tweets">
    <div class="title">
        <div class="wrapper">
        <h5><i class="fab fa-twitter"></i>
            <?= $instance['title'] ?></h5>
        </div>

        <a href="#userlink">
            Siga a @uneoficial
        </a>
    </div>

    <div class="content">
        <a class="twitter-timeline" data-tweet-limit="3" data-height="274" href="https://twitter.com/uneoficial?ref_src=twsrc%5Etfw">Tweets by uneoficial</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>

</section>

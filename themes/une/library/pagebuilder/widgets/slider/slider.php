<?php
/*
  Widget Name: Slider de comprimento completo
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class Slider extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'repetidor_bloco' => array(
                'type' => 'repeater',
                'label' => __( 'Itens' , 'une'),
                'fields' => array(

                    'background' => array(
                        'type' => 'media',
                        'label' => __('Background', 'une' ),
                        'library' => 'image',
                    ),

                    'background_mobile' => array(
                        'type' => 'media',
                        'label' => __('Background mobile', 'une' ),
                        'library' => 'image',
                    ),

                    'chapeu' => array(
                        'type' => 'text',
                        'label' => __( 'Chapéu', 'une')
                    ),

                    'link' => array(
                        'type' => 'text',
                        'label' => __( 'Endereço de destino', 'une')
                    ),

                    
                )
            )
        ];

        parent::__construct('slider', 'Slider de comprimento completo', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('slider', __FILE__, 'widgets\Slider');

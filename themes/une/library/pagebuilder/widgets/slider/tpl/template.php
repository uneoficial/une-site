
<section class="slider-section" data-flickity>
    <?php
        $suze = count($instance['repetidor_bloco']);
    ?>

    <?php foreach ($instance['repetidor_bloco'] as $block): ?>
        <div class="block carousel-cell slider-item" data-background="<?= wp_get_attachment_url($block['background_mobile']) ?>" style="background-image: url(<?= wp_get_attachment_url( $block['background'] ); ?>">            

            <a href="<?= $block['link'] ?>" class="chapeu">
                <?= $block['chapeu'] ?>
            </a>

        </div>
    <?php endforeach; ?>
</section>

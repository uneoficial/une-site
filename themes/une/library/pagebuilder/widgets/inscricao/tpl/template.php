<?php 
    global $flag;


    if ( isset( $_POST['cpt_nonce_field'] ) && wp_verify_nonce( $_POST['cpt_nonce_field'], 'cpt_nonce_action' ) && !( isset( $flag )?: false )  ) {
        if ( ! isset( $flag )) {
            $flag = true; // bloquear para rodar apenas uma vez
         }

        $post_args = array(
            'post_title'    => $_POST['nome_atividade'],
            'post_status'   => 'pending',
            'post_type' => 'atividades',

        );

        $post_id = wp_insert_post( $post_args, $wp_error);

        wp_set_object_terms( $post_id, $instance['campanha'], 'campanha' );

        update_post_meta( $post_id, 'nome_completo', $_POST['nome_completo'] );
        update_post_meta( $post_id, 'email', $_POST['email'] );
        update_post_meta( $post_id, 'telefone', $_POST['telefone'] );
        update_post_meta( $post_id, 'uf', $_POST['estado'] );
        update_post_meta( $post_id, 'cidade', $_POST['cidade'] );
        update_post_meta( $post_id, 'entidade_prepositora', $_POST['entidade_prepositora'] );
        update_post_meta( $post_id, 'universidade', $_POST['universidade'] );
        update_post_meta( $post_id, 'tipo', $_POST['tipo'] );
        update_post_meta( $post_id, 'data', $_POST['data'] );
        update_post_meta( $post_id, 'horario', $_POST['hora'] );

    }

?>


<section class="inscricao">

    <h1 class="title">
        <?= $instance['title'] ?>
    </h1>

    <div class="instructions">
        <div class="">
            <div class="instructions--header">
                <div class="number">1.</div>
                <div class="text">
                    <?= $instance['title_form_0'] ?>
                    <div class="form">

                    </div>
                </div>
            </div>  
        </div>
        <div class="">
            <div class="instructions--header">
                <div class="number">2.</div>
                <div class="text">
                    <?= $instance['title_form_0'] ?>
                    <div class="form">

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="form">
        <form action="#success" method="POST" class="js-estados-e-municipios">
            <div>
                <input type="text" name="nome_completo" required placeholder="NOME COMPLETO">
                <input type="email" name="email" required  placeholder="EMAIL">
                <input type="text" name="telefone" required  class="telefone" placeholder="DDD + Telefone">
            
                <div class="select-wrapper">
                    <select class="js-estados js-copy-value" required  name="estado" id="mauticform_junteseagora_estado" ></select>
                    <select class="js-municipios js-copy-value" required  name="cidade" id="mauticform_junteseagora_cidade" ></select>
                </div>

                <input type="text" name="entidade_prepositora" required placeholder="Entidade Prepositora">
                <input type="text" name="universidade" required placeholder="Universidade">
            
            </div>

            <div>
                <div class="radio-wrapper">
                    <label for="festival" class="container">festival
                        <input type="radio" id="festival" name="tipo" value="festival">
                        <span class="checkmark"></span>
                    </label>
                    
                    <label for="debate" class="container">debate
                        <input type="radio" id="debate" name="tipo" value="debate">
                        <span class="checkmark" class="container"></span>
                    </label>
                    
                    <label for="aula_de_rua" class="container">aula de rua
                        <input type="radio" id="aula_de_rua" name="tipo" value="aula_de_rua">
                        <span class="checkmark"></span>
                    </label>

                    <label for="outro" class="container">
                        <span class="changeble">outro</span>
                        <input type="radio" id="outro" name="tipo" value="outro">
                        <span class="checkmark"></span>
                    </label>
                </div>

                <input type="text" id="outro_tipo" name="outro_tipo" placeholder="diga o que">
                <input type="text" id="nome_atividade" required  name="nome_atividade" placeholder="Nome da atividade">
                <input type="date" id="data" name="data" placeholder="selecione uma data" required >
                <input type="time" id="hora" name="hora" placeholder="horário" required >

            </div>

            <div class="checkbox-wrapper">
                <input type="checkbox" id="terms" name="terms" value="compromisso" required>
                <label for="terms"> 
                    Concordo com os <a href="<?= $instance['link_termos'] ?>">termos de compromisso</a> da campanha 
                </label>
                
            </div>

            <?php wp_nonce_field( 'cpt_nonce_action', 'cpt_nonce_field' ); ?>

            <div class="submit-wrapper" id="success">
                <input type="submit" value="Enviar">
            </div>

            <?php 
            
            if ( isset( $_POST['cpt_nonce_field'] ) && wp_verify_nonce( $_POST['cpt_nonce_field'], 'cpt_nonce_action' )  ) : ?>
                <div class="sucess" id="sucesso">
                    Sucesso. Obrigado por sua solicitação! Logo mais estaremos analisando-a.
                </div>
            <?php endif; ?>
        </form>    
    </div>
</section>

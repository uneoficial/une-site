<?php
/*
  Widget Name: Área de Inscrição
  Description:
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class Inscricao extends \SiteOrigin_Widget {

    function __construct() {

        $filtered_terms = [];

        $terms = get_terms( 'campanha', array(
            'hide_empty' => false,
        ) );

        foreach ($terms as $term) {
            $filtered_terms[$term->slug] = $term->name;
        }

        $fields = [

            'campanha' => array(
                'type' => 'select',
                'label' => __( 'Selecione uma campanha para associar', 'une' ),
                'multiple' => false,
                'options' => $filtered_terms,
                'required' => true,
            ),

            'title' => array(
                'type' => 'text',
                'label' => __('Título da seção', 'une'),
            ),

            'title_form_0' => array(
                'type' => 'text',
                'label' => __('Título formulário parte 1', 'une'),
            ),

            'title_form_1' => array(
                'type' => 'text',
                'label' => __('Título formulário parte 2', 'une'),
            ),

            'link_termos' => array(
                'type' => 'text',
                'label' => __('Link termos de uso', 'une'),
            ),

        ];

        parent::__construct('inscricao', 'Área de Inscrição', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Listagem de tweets'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('inscricao', __FILE__, 'widgets\Inscricao');

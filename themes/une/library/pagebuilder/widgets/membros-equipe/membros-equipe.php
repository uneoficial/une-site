<?php
/*
  Widget Name: Membros da Equipe
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class MembrosEquipe extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            // 'repetidor_membro' => array(
            //     'type' => 'repeater',
            //     'label' => __( 'Itens' , 'une'),
            //     'fields' => array(
            //         'name' => array(
            //             'type' => 'text',
            //             'label' => __( 'Nome do membro', 'une')
            //         ),

            //         'what' => array(
            //             'type' => 'text',
            //             'label' => __( 'Papel', 'une')
            //         ),

            //         'description' => array(
            //             'type' => 'tinymce',
            //             'label' => __( 'Descrição', 'une' ),
            //             'rows' => 10,
            //             'default_editor' => 'text',
            //             'button_filters' => array(
            //                 'mce_buttons' => array( $this, 'filter_mce_buttons' ),
            //                 'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
            //                 'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
            //                 'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
            //                 'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
            //             ),
            //         ),
                    
            //         'avatar' => array(
            //             'type' => 'media',
            //             'label' => __( 'Imagem', 'une' ),
            //             'choose' => __( 'Escolha uma imagem', 'une'),
            //             'update' => __( 'Defina uma imagem', 'une'),
            //             'library' => 'image',
            //             'fallback' => true
            //         )

            //     )
            // )
        ];

        parent::__construct('membros-equipe', 'Membros da Equipe', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => ''
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('membros-equipe', __FILE__, 'widgets\MembrosEquipe');

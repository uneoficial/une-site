<div id="single-the-title"><h1>Diretoria</h1></div>
<section class="members-section widget">
<?php
    $argsv = array(  
        'post_type' => 'diretoria',
        'post_status' => 'publish',
        'posts_per_page' => -1, 
    );

    $loop = new WP_Query( $argsv ); 
        
    while ( $loop->have_posts() ) : $loop->the_post();  ?>

        <div class="member <?= strlen(get_the_title()) > 18? 'huge-name' : null ?>">
            <div class="pic">
                <img src="<?= wp_get_attachment_url(get_post_meta( get_the_ID(), 'diretoria_foto', true )); ?>" alt="<?php the_title() ?>" class="avatar">
            </div>
            <div class="data">
                <div class="name"><?php the_title() ?></div>
                <div class="what"><?= get_post_meta( get_the_ID(), 'diretoria_cargo', true ) ?></div>
                <div class="social">
                    <?php if( get_field('diretoria_twitter') != ''): ?>
                        <a href="https://twitter.com/<?= get_field('diretoria_twitter')?>">
                            <i class="fab fa-twitter"></i>
                        </a>
                    <?php endif; ?>
                </div>
                <!-- <div class="description"><?= get_post_meta( get_the_ID(), 'diretoria_profissao_curso', true ) ?></div> -->
            </div>
        </div>

    <?php
    endwhile;

    wp_reset_postdata();  ?>

    <a href="#" class="see-all"> Ver todos </a>

</section>

<section class="card-section">
    <?php
        $suze = count($instance['repetidor_bloco']);
    ?>

    <?php foreach ($instance['repetidor_bloco'] as $block): ?>
        <div class="block-card <?= $suze > 1? 'large-4 medium-4' : '' ?> <?= $block['background_selection']?>">
            <span class="icon" style="background-image: url(<?= wp_get_attachment_url($block['icon']); ?>"></span>
            
            <div class="title"> 
                <?= strip_tags($block['title']) ?>
            </div>

            <div class="info">
                <div class="content">
                    <?= $block['description'] ?>
                </div>
            </div>

            <div class="buttons">
                <a class="button" href="<?= $block['button_url']; ?>" target="_blank"><?= $block['button_text']; ?></a>
            </div>
        </div>
    <?php endforeach; ?>
</section>

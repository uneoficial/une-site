<?php
/*
  Widget Name: Cartões de Notas
  Description: 
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class CardNotas extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
            'repetidor_bloco' => array(
                'type' => 'repeater',
                'label' => __( 'Itens' , 'une'),
                'fields' => array(
                    'icon' => array(
                        'type' => 'media',
                        'label' => __('Icone do cartão', 'une' ),
                        'library' => 'image',
                    ),

                    'title' => array(
                        'type' => 'text',
                        'label' => __( 'Título', 'une')
                    ),
                    'description' => array(
                        'type' => 'tinymce',
                        'label' => __( 'Descrição', 'une' ),
                        'rows' => 10,
                        'default_editor' => 'text',
                        'button_filters' => array(
                            'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                            'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                            'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                            'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                            'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                        ),
                    ),

                    'button_text' => array(
                        'type' => 'text',
                        'label' => __('Texto do botão', 'une'),
                        'default' => 'ver linha do tempo'
                    ),

                    'button_url' => array(
                        'type' => 'text',
                        'label' => __('Link do botão', 'une'),
                        'default' => 'http://www.example.com'
                    ),

                    'background_selection' => array(
                        'type' => 'select',
                        'label' => __( 'Escolha uma opção de gradiente para o fundo', 'une' ),
                        'default' => 'green_blue',
                        'options' => array(
                            'green_blue' => __( 'Verde e Azul', 'une' ),
                            'purple_blue' => __( 'Roxo e Azul', 'une' ),
                            'blue_pink' => __( 'Azul e Rosa', 'une' ),
                        )
                    ),

                    
                )
            )
        ];

        parent::__construct('card-notas', 'Cartões de Notas', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Cartões de Notas'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('card-notas', __FILE__, 'widgets\CardNotas');

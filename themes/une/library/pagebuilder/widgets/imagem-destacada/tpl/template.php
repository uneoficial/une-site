
<section class="image-destacada">
    <a href="<?= $instance['url'] ?>" target="_blank">
        <div class="mobile-image">
            <?= wp_get_attachment_image($instance['imagem_mobile'], 'full') ?>
        </div>

        <div class="desktop-image">
            <?= wp_get_attachment_image($instance['imagem'], 'full') ?>
        </div>
    </a>

</section>

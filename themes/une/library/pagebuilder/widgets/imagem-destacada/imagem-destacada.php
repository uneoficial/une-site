<?php
/*
  Widget Name: Imagem Destacada
  Description: Imagem destacada com gradiente no fundo
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class ImagemDestacada extends \SiteOrigin_Widget {

    function __construct() {

        $fields = [
                
            'imagem' => array(
                'type' => 'media',
                'label' => __('Imagem', 'une' ),
                'library' => 'image',
            ),

            'imagem_mobile' => array(
                'type' => 'media',
                'label' => __('Imagem Mobile', 'une' ),
                'library' => 'image',
            ),

            'url' => array(
                'type' => 'link',
                'label' => __('URL de destino', 'une'),
                'default' => 'http://www.example.com'
            ),
        ];

        parent::__construct('imagem-destacada', 'Imagem Destacada', [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Imagem destacada com gradiente no fundo'
        ], [], $fields, plugin_dir_path(__FILE__));
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }

}

Siteorigin_widget_register('imagem-destacada', __FILE__, 'widgets\ImagemDestacada');

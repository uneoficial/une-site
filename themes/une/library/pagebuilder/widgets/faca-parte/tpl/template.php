
<div class="formulario-widget">

    <div class="formulario-widget--header">
        <i class="icon fab fa-font-awesome-flag"></i>

        <div class="title">
            <?= $instance['title'] ?>
        </div>

        <div class="description">
            <?= $instance['description'] ?>
        </div>
        
    </div>
    
    <?= do_shortcode($instance['widget_shortcode']) ?>
    
</div>

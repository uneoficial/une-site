<?php

/*
  Widget Name: Área faça parte
  Description: Widget com titulo e area para inserção de formulário.
  Author: hacklab/
  Author URI: https://hacklab.com.br/
 */

namespace widgets;

class FacaParte extends \SiteOrigin_Widget {

    function __construct() {
        $fields = [
            'title' => array(
                'type' => 'text',
                'label' => __('Título', 'une'),
            ),

            'description' => array(
                'type' => 'tinymce',
                'label' => __( 'Descrição', 'une' ),
                'rows' => 10,
                'default_editor' => 'text',
                'button_filters' => array(
                    'mce_buttons' => array( $this, 'filter_mce_buttons' ),
                    'mce_buttons_2' => array( $this, 'filter_mce_buttons_2' ),
                    'mce_buttons_3' => array( $this, 'filter_mce_buttons_3' ),
                    'mce_buttons_4' => array( $this, 'filter_mce_buttons_5' ),
                    'quicktags_settings' => array( $this, 'filter_quicktags_settings' ),
                ),
            ),

            'widget_shortcode' => array(
                'type' => 'text',
                'label' => __('Shortcode formulário', 'une'),
            ),
        ];

        parent::__construct('faca-parte', "Área faça parte", [
            'panels_groups' => [WIDGETGROUP_MAIN],
            'description' => 'Widget com titulo e area para inserção de formulário.'
                ], [], $fields, plugin_dir_path(__FILE__)
        );
    }

    function get_template_name($instance) {
        return 'template';
    }

    function get_style_name($instance) {
        return 'style';
    }


}

Siteorigin_widget_register('faca-parte', __FILE__, 'widgets\FacaParte');

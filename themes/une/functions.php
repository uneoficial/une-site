<?php
namespace une;

require __DIR__ . '/library/settings.php';
require __DIR__ . '/library/images.php';
require __DIR__ . '/library/menus.php';
require __DIR__ . '/library/assets.php';
require __DIR__ . '/library/widgets.php';
require __DIR__ . '/library/custom-posts.php';
require __DIR__ . '/library/taxonomies.php';
require __DIR__ . '/library/pagebuilder/index.php';
// descomentar se for um multisite e for utilizar o plugin de sincronização de posts
// require __DIR__ . '/library/mssync.php';

add_theme_support( 'align-wide' );
add_theme_support( 'custom-logo' );
add_theme_support( 'post-thumbnails' );

// descomentar para importar conteúdo
// add_filter( 'force_filtered_html_on_import' , '__return_false' );

// removemos a barra de admin para não cachear no cache de borda 
add_filter( 'show_admin_bar', '__return_false' );

// usamos uma taxonomia autor criada com o plugin Pods
add_action( 'init',  function () {
	remove_post_type_support( 'post', 'author' );
});

// filtro para adicionar mais classes de título
add_filter('widgets\\SectionTitle:css_classes', function($css_classes){
	$css_classes['title-une'] = __('Título une', 'une');
	return $css_classes;
});

function get_archive_title () {
	$s = isset($_GET['s']) ? trim($_GET['s']) : '';

	if($s){
		if (is_tag()) {
			$title = sprintf(__('Busca por "%s" na tag "%s"', 'une'), $s, single_tag_title('',false));
		} elseif (is_category()) {
			$title = sprintf(__('Busca por "%s" na categoria "%s"', 'une'), $s, single_cat_title('',false));
		} elseif (is_tax()) {
			$title = sprintf(__('Busca por "%s" em "%s"', 'une'), $s, single_term_title('',false));
		} else {
			if(is_archive()){
				$title = sprintf(__('Busca por "%s" em "%s"', 'une'), $s, post_type_archive_title());
			} else {
				$title = sprintf(__('Resultado da busca por "%s"', 'une'), $s);
			}
		}
	} else {
		if (is_tag()) {
			$title = single_tag_title('Tag: ',false);
		} elseif (is_category()) {
			$title = single_cat_title('',false);
		} elseif (is_tax()) {
			$title = single_term_title('',false);
		} else {
			$title = post_type_archive_title( '', false );
		}
	}

	return $title;
}

/**
* Pagination
*/
function pagination_links() {
    global $wp_query;
    if ( $wp_query->max_num_pages > 1 ) {

        $big = 999999999; // need an unlikely integer

        echo paginate_links( array(
            'base' => str_replace( $big , '%#%' , esc_url( get_pagenum_link( $big ) ) ) ,
            'format' => '?paged=%#%' ,
            'current' => max( 1 , get_query_var( 'paged' ) ) ,
            'total' => $wp_query->max_num_pages ,
            'prev_text' => __( '←' , 'green' ) ,
            'next_text' => __( '→' , 'green' )
        ) );
    }
}

//remove_filter('the_content', 'wpautop');


function template_part($template_name, $params = []){
    global $wp_query;
    $template_filename = locate_template("template-parts/{$template_name}.php");

    if(empty($template_filename)){
        return;
    }

    extract($params);

    include $template_filename;;
}


add_filter('pre_get_posts', 'une\_search_pre_get_posts',100);

function _search_pre_get_posts($query){
    global $wp_query;
    //var_dump();

    if(is_admin()){
        return $query;
    }
    if(isset($query->query['p']) && strpos($query->query['p'], ':redirect') > 0){
        $query->query['p'] = intval($query->query['p']);
        $query->is_404 = false;
        $query->is_page = true;
        $query->is_home = false;
    }


    if( $query->is_main_query() && $query->is_search() && $query->is_archive() ){
        $query->set('post_type', [$query->query['post_type']]);
    }
    return $query;
}


if(!get_option('fix-excerpt-and-post-images')){
    add_option('fix-excerpt-and-post-images', 1);
    global $wpdb;

    $wpdb->query("UPDATE $wpdb->posts p SET p.post_excerpt = (SELECT meta_value FROM $wpdb->postmeta WHERE post_id = p.ID AND meta_key = 'noticias_resumo') WHERE post_type = 'noticias' AND ID IN (SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'noticias_resumo')");
    $wpdb->query("UPDATE $wpdb->postmeta SET meta_key = '_thumbnail_id' WHERE meta_key = 'noticias_imagem'");
    $wpdb->query("UPDATE $wpdb->posts p SET p.post_excerpt = (SELECT meta_value FROM $wpdb->postmeta WHERE post_id = p.ID AND meta_key = 'tvUne_resumo') WHERE post_type = 'tv-une' AND ID IN (SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'tvUne_resumo')");
    $wpdb->query("UPDATE $wpdb->posts p SET p.post_excerpt = (SELECT meta_value FROM $wpdb->postmeta WHERE post_id = p.ID AND meta_key = 'linha_fina_blog') WHERE ID IN (SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'linha_fina_blog') AND post_excerpt = ''");
}

if(!get_option('fix-post-images')) {
    add_option('fix-post-images', 1);
    
    global $wpdb;
    $wpdb->query("UPDATE $wpdb->postmeta SET meta_key = '_thumbnail_id' WHERE meta_key = 'thumb_blog'");
}

function the_category_filter($thelist,$separator=' ') {  
    if(!defined('WP_ADMIN')) {  
        //Categories to exclude  
        $exclude = array('Uncategorized', 'Private', 'Sem categoria');  
          
        $cats = explode($separator,$thelist);  
        $newlist = array();  
        foreach($cats as $cat) {  
            $catname = trim(strip_tags($cat));  
            if(!in_array($catname,$exclude))  
                $newlist[] = $cat;  
        }  
        return implode($separator,$newlist);  
    } else {  
        return $thelist;  
    }  
}  

add_filter('the_category','une\the_category_filter', 10, 2);  

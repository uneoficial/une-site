<?php
get_header();
?>

<div class="row mt-20 pt-60">
    <?php 
        global $wp_query;
        $type = $wp_query->get('post_type');

    ?>

    <div class="column medium-9 small-12">
        <?php 

            guaraci\template_part('archive-title', ['title' => une\get_archive_title()]);
            
            if ($type[0] == "mocoes-e-resolucoes") {
                guaraci\template_part('mocoes-filters');
                guaraci\template_part('posts-list', ['show_date' => false, 'show_category' => true, 'horizontal' => false, 'card' => 'card-moncoes', 'show_excerpt' => true]);
            } else if ($type[0] == "publicacoes") {
                guaraci\template_part('publicacoes-filters');
                guaraci\template_part('posts-list', ['show_date' => false, 'show_category' => true, 'horizontal' => false, 'card' => 'card-publicacao', 'show_excerpt' => true]);
            } else {
                guaraci\template_part('posts-list', ['show_date' => false]);
            }
        ?>
        
    </div>

    <div class="column medium-3 small-12 mt-20 mb-20 archive-sidebar">
        <?php guaraci\template_part('sidebar-widgets'); ?>
    </div>
</div>

<?php get_footer();
